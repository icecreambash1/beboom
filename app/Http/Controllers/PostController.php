<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function index(Request $request) {
        return DB::select("SELECT * FROM posts");
    }

    public function store(Request $request) {
        $data = $request->only(
            [
                'name',
                'test',
            ]
        );

        return DB::insert("INSERT INTO posts (name,test) VALUES (?,?)",[$data['name'],$data['test']]);
    }

    public function destroy($id) {
        return DB::delete("DELETE FROM posts WHERE id = ?",[$id]);
    }

    public function update(Request $request) {

        $payload = $request->only(
            [
                'name',
                'test'
            ]
        );

        return DB::update('UPDATE posts SET name=?,test=? where id =?',
        [
            $request->name,
            $request->test,
            $request->id,
        ]
        );
    }
}
